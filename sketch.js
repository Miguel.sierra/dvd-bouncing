let rad = 80; // Width of the shape
let xpos, ypos; // Starting position of shape

let xspeed = 2.8; // Speed of the shape
let yspeed = 2.2; // Speed of the shape

let xdirection = 1; // Left or Right
let ydirection = 1; // Top to Bottom

let dvd;

function preload(){
  dvd = loadImage("https://raw.githubusercontent.com/CodingTrain/website/master/CodingChallenges/CC_131_BouncingDVDLogo/P5/dvd_logo.png");
}

function setup() {
  createCanvas(windowWidth, windowHeight);
  //noStroke();
  frameRate(30);
  //ellipseMode(RADIUS);
  // Set the starting position of the shape
  xpos = width / 2;
  ypos = height / 2;
}

function pickColor() {
  r = random(255);
  g = random(255);
  b = random(255);
  tint(r,g,b);
}

function draw() {
  background(0);

  // Update the position of the shape
  xpos = xpos + xspeed * xdirection;
  ypos = ypos + yspeed * ydirection;

  // Test to see if the shape exceeds the boundaries of the screen
  // If it does, reverse its direction by multiplying by -1
  /*
  if (xpos > width - rad || xpos < rad) {
    xdirection *= -1;
	  r = random(255);
	  g = random(255);
	  b = random(255);
	  tint(r,g,b);

  }
  if (ypos > height - rad || ypos < rad) {
    ydirection *= -1;
    r = random(255);
    g = random(255);
    b = random(255);
    tint(r,g,b);

  }*/


  if (xpos + dvd.width >= width) {
    xdirection *= -1;
    xpos = width - dvd.width;
    pickColor();
  } else if (xpos <= 0) {
    xdirection *= -1;
    xpos = 0;
    pickColor();
  }

  if (ypos + dvd.height >= height) {
    ydirection *= -1;
    ypos = height - dvd.height;
    pickColor();
  } else if (ypos <= 0) {
    ydirection *= -1;
    ypos = 0;
    pickColor();
  }

  // Draw the shape
  //ellipse(xpos, ypos, rad, rad);

  image(dvd,xpos,ypos,rad);
}